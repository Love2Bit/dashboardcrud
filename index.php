<?php include 'inc/connection.php'; ?>

<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <style>
      body {
        background-image: url(images/bg.jpg);
        background-repeat: no-repeat;
        background-position: center;
        max-width: 100%;
        max-height: 100%;
      }
      a {
        color: white;
      }
      a:hover {
        color: darkorange;
      }
      .scrollbar
      {
        height: 260px;
        overflow-y: scroll;
        box-shadow: 5px 10px 5px black inset;
      }
      #overflow::-webkit-scrollbar-track
      {
        -box-shadow: inset 0 0 6px rgba(0,0,0,0.9);
        border-radius: 10px;
        background-color: #CCCCCC;
      }
      
      #overflow::-webkit-scrollbar
      {
        width: 12px;
        background-color: white;
      }
      
      #overflow::-webkit-scrollbar-thumb
      {
        border-radius: 10px;
        background-color: rgb(18, 0, 177);
        background-image: -webkit-linear-gradient(90deg,
        transparent,
        rgba(0, 0, 0, 0.4) 50%,
        transparent,
        transparent)
      }
    </style>
    <title>Dashboard Item</title>
  </head>
  <body>
  

    <!-- Image and text -->
    <nav class="navbar navbar-dark bg-dark rounded mt-2" >
      <a class="navbar-brand text-center" href="#">
        <img src="images/logo.jpg" width="50" height="50" class="d-inline-block align-top" alt="" loading="lazy">
        <p class="float-right font-weight-bold mt-2">Item's</p>
      </a>
      <img src="images/logo.jpg" width="60" height="auto" alt="">
    </nav>
  <div class="container">
    <nav class="navbar navbar-expand-md bg-transparent border-top mt-3">
      <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
        <div class="navbar-nav">
          <a class="nav-link active font-weight-bold" href="#" style="color: darkorange;">Home</a>
        </div>
      </div>
    </nav>
    <div class="row">
      <div class="col-sm-12 col-md-6 col-lg-6">
        <div class="row">
          <div class="col-sm-12 col-md-6 col-lg-6">
            <div class="card bg-dark">
              <div class="row no-gutters">
                <div class="col-sm-12 col-md-4 col-md-4">
                  <img src="images/legend.png" class="card-img mt-2" alt="...">
                  <h6 class="text-light text-center mt-1">In Game Item's</h6>
                </div>
                <div class="col-sm-12 col-md-8 col-lg-8">
                  <div class="card-body p-3">
                    <p><small class="text-light">is an item that you can sell it for real life money</small></p>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-sm-12 col-md-6 col-lg-6">
            <div class="card bg-dark">
              <div class="row no-gutters">
                <div class="col-sm-12 col-md-8 col-lg-8">
                  <div class="card-body" style="padding: 10px;">
                    <p><small class="text-light">Legendary Item</small></p>
                  </div>
                </div>
                <div class="col-sm-12 col-md-4 col-md-4">
                  <img class="my-2" src="images/legend.png" alt="" width="80px">
                </div>
              </div>
            </div>               
          </div>
        </div>
        <div class="card-body scrollbar p-0 mt-2" id="overflow">
          <table class="table table-hover">
            <thead class="thead-dark">
              <tr id="text-card">
                <th scope="col">Name</th>
                <th scope="col">Rarity</th>
                <th scope="col">Price</th>
              </tr>
            </thead> 
            <?php foreach($data_player as $key): ?>
            <tbody class="text-light" style="background-color: firebrick;">
              <tr>
                <td class="font-weight-bold"><?php echo $key['nama']; ?></td>
                <td class="font-weight-bold"><?php echo $key['rarity']; ?></td>
                <td class="font-weight-bold"><?php echo "Rp.".number_format($key['price']); ?></td>
              </tr>
            </tbody>
            <?php endforeach; ?>
          </table>
        </div>
      </div>
  </div>
    
    
    <!-- Optional JavaScript; choose one of the two! -->
    
    <!-- Option 1: jQuery and Bootstrap Bundle (includes Popper) -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
    
    <!-- Option 2: jQuery, Popper.js, and Bootstrap JS
      <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
      <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
      <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.min.js" integrity="sha384-w1Q4orYjBQndcko6MimVbzY0tgp4pWB4lZ7lr30WKz0vr/aWKhXdBNmNb5D92v7s" crossorigin="anonymous"></script>
    -->
  </body>
</html>
